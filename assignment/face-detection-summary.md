# FACE DETECTION

Face detection is an AI-based computer technology which is used to locate and detect human faces in images and videos. It has multiple uses and is being used all around us. Face detection is essential part of face tracking, analysis and recognition operations. Face Detection can be thought of a specialization of object detection. It is used in security cameras (real time surveillance), biometrics, applications like Facebook to tag persons, etc.

## Face Detection Methods
Face detection methods of face detection in pictures is complicated because the variations in expressions, skin colour, facial hair, scarfs, position and orientation among many others. These methods can be divided into the following four categories (non exclusive) :

* Knowledge-based : Detects a face based on a set of rules. The difficulty lies in deciding these rules challenge of this approach is the difficulty of coming up with well-defined rules. There’s triggering of many false positive if the rules were too general or too detailed and hence, not is unsuffiencent if deployed alone.
* Feature invariant methods : Detects a face by extracting structural features (like eyes, nose, etc.) of the face. It’s trained as a classifier and then used by differentiating between facial and non-facial regions. It reports a success rate of 94%. 
* Template-matching methods : Detects a face by comparing images with standard predefined face and feature templates. Unfo	rtunately, they don’t address the variations in pose, scale and shape.
* Appearance-based methods employ statistical analysis and machine learning to find the relevant characteristics of face images. This method, also used in feature extraction for face recognition, better than the other methods, is divided into many sub-methods.  

## Working of Face Detection
There are many techniques and the face detection procedure is almost similar in all, such as OpenCV, Neural Networks, Matlab, etc. Here’s an example based on OpenCV and the steps are as follows : 

* Image is transformed from RGB to Grayscale( to ease the detection).  
![fd](images/fd.png)
* Resizing, cropping, blurring and sharpening of the images  is done (if needed.) 
* Image segmentation is done to aid contour detection. It segments multiple objects in an image so as to make it easier for the classifier to detect objects and faces.
* Haar-Like features algorithm, proposed by Voila and Jones is used for face detection in a frame. Universal properties of the human face like the eyes region is darker than its neighbour pixels, nose region is brighter than eye region, etc. are used.  
![fe](images/fe.png)
* It’s also used for feature extraction using edge detection, line detection, centre detection for detecting eyes, nose, mouth, etc. in the picture, which can be used for face detection.
* Coordinates are provided which makes a rectangle box in the picture to show the location of the face. Many other detection techniques like smile detection, eye detection, blink detection, etc. are used in combination as well.  
![ff](images/ff.png)


Although the _Viola-Jones_ framework is still popular for face recognition in real-time applications, it struggles if a face is covered with a mask or scarf, etc. To overcome these issues other algorithms like _Region-based Convolutional Neural Network_ (R-CNN) and _Single Shot Detector_ (SSD) have been developed.  
CNN is an artificial neural network used in image recognition and processing that is specifically designed to process pixel dataand R-CNN generates region proposals on a CNN framework to localize and classify objects in images.  

R-CNN (and other region proposal network-based approaches) need two shots : one for generating region proposals and one for detecting the object of each proposal. Meanwhile, SSD only requires one shot to detect multiple objects within the image.  
Therefore, SSD is significantly faster than R-CNN.  
![fg](images/fg.png)

