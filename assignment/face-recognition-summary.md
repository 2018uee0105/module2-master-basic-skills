## FACE RECOGNITION

**Face recognition** is a biometric identification technology capable of identifying or verifying a subject through an image, video or any audio-visual element of his face. Subject is usually identified and their verified/authenticated using its facial biometric pattern and data. Biometric facial recognition is  considered to be safer and more effective than its counterparts, like passwords , fingerprints, etc., as it uses unique mathematical and dynamic patterns for identification.  
![fu](images/fu.jpg)  

The facial recognition involves 2 major processes :  
1. **Digital Onboarding** : When the system encounters a face for the first time, it registers it and associates it with an identity, which is then stored in the system.  
2. **Authentication** : The incoming image data is cross-verified with the existing data in the database. If it matches an already registered identity, the user is granted access to the system or is registered in the system (according to the use).

#### WORKING OF FACE RECOGNITION 

1. A picture of your face is captured from a photo or video ( i.e. _Face Detection_). Face could appear alone / in crowd, looking straight or in other recorded profile.  
![fs](images/fs.jpg)
2. Recognition software works on the _face geometry_. Key factors include the distance between your eyes and the distance from forehead to chin. The software identifies facial landmarks — one system identifies 68 of them — that are key to distinguishing your face. The result: your facial signature.  
![fr](images/fr.jpg)
3. The _facial signature_, a mathematical formula, is compared to a database of known faces and a final determination is made.

#### SOME USES OF FACIAL RECOGNITION

* Used for security at airports, entrances, retailing shops, restricted areas etc. 
* Used for face unlocking in smartphones and other devices.  
![ft](images/ft.jpg)
* Used to check attendance in colleges classrooms. 
* Used to tag and predict profiles while posting an image to a Social media. 

